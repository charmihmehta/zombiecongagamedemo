//
//  GameScene.swift
//  ZombieConga
//
//  Created by Parrot on 2019-01-29.
//  Copyright © 2019 Parrot. All rights reserved.hjjhh
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
   
    override func didMove(to view: SKView) {
        // Set the background color of the app
        self.backgroundColor = SKColor.black;
        
        // Add background
        let background = SKSpriteNode(imageNamed: "background1")
        
        //Configur the background
        background.position = CGPoint(x: self.size.width/2,y: self.size.height/2)
        
        addChild(background)
        
        
        // add a zombie
        let zombie = SKSpriteNode(imageNamed: "zombie1")
        
        //configur zombie position
        zombie.position = CGPoint(x:400,y:400)
        //addd to child
        addChild(zombie)
    }
    
}
